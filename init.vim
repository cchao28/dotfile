call plug#begin('~/.vim/plugged')
Plug 'nathanaelkane/vim-indent-guides'
Plug 'bronson/vim-trailing-whitespace'

Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'

Plug 'ervandew/supertab'
Plug 'valloric/youcompleteme'
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'

Plug 'kchmck/vim-coffee-script'
Plug 'rust-lang/rust.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'chriskempson/base16-vim'

Plug 'google/vim-maktaba'
Plug 'google/vim-codefmt'
Plug 'google/vim-glaive'
Plug 'google/vim-searchindex'
call plug#end()

set nu ruler mouse=a ai ts=2 sw=2 sts=2 et cc=80
auto FileType python set sw=4 sts=4
auto FileType makefile set noet

au VimEnter * IndentGuidesEnable

map <C-n> :NERDTreeToggle<CR>

let g:solarized_termcolors=256
set termguicolors

syntax enable
set background=dark
colorscheme base16-monokai
let g:airline_theme='base16_monokai'

map <C-x> <ESC>:!pbcopy < %<CR>
map <C-c> <ESC>:!bd %<CR>

set foldmethod=marker
au BufNewFile *.cpp 0r ~/.vim/skeleton.cpp

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
let g:UltiSnipsExpandTrigger = "<tab>"
let g:UltiSnipsJumpForwardTrigger = "<tab>"
let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"
